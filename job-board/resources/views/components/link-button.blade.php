<a href="{{ $href }}" class="rounded-md border border-slate-300 bg-white px-1 py-1 text-center text-xs hover:bg-slate-100">
    {{ $slot }}
</a>
